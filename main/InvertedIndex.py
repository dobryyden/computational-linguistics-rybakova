from lxml import etree
import nltk
from nltk.corpus import stopwords
import pymorphy2

def sentences_from_xml(file):
    sentences = []
    tree = etree.parse(file)
    contents = tree.xpath('/Body/Sentence')
    for content in contents:
        sentences.append(content.text)
        print(content.text)
    return sentences

def normalize_words(words):
    morph = pymorphy2.MorphAnalyzer()
    stop_words = stopwords.words('russian')
    stop_words.extend(['что', 'это', 'так', 'вот', 'быть', 'как', 'в'])
    tokens = [i for i in words if (i not in stop_words)]
    norm_words = []
    for word in tokens:
        norm_words.append(morph.parse(word)[0].normal_form)
    return norm_words

def invert_index(sentences):
    inverted_index = {}
    sent_number = 0;
    for sent in sentences:
        tokens = nltk.word_tokenize(sent)
        words = normalize_words(tokens)
        words = set(words)
        for word in words:
            if inverted_index.get(word):
                inverted_index.get(word).append(sent_number)
            else:
                inverted_index.update({word: [sent_number]})
        sent_number += 1
    return inverted_index

file = 'culture.xml'
#file = 'health.xml'
sentences = sentences_from_xml(file)
inverted_index = invert_index(sentences)

while True:
    query = input("Enter search query:\n")
    words = normalize_words(nltk.word_tokenize(query))
    result = {}
    for word in words:
        found_words = inverted_index.get(word)
        if found_words:
            if len(result) == 0:
                result = set(found_words)
            else:
                result = result.intersection(found_words)
    print("\nThis words in sentences: ", result)
    for i in result:
        print(sentences[i])
