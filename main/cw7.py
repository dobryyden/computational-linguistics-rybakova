from lxml import etree
import codecs
from treetaggerwrapper import TreeTagger

tt = TreeTagger(TAGLANG='ru', TAGDIR='C:\TreeTagger')


def sentences_from_xml(file):
    sentences = []

    tree = etree.parse(file)
    contents = tree.xpath('/Body/Sentence')
    for content in contents:
        sentences.append(content.text)
        print(content.text)
    return sentences


file = 'culture.xml'
sentences = sentences_from_xml(file)
f = codecs.open('tagged.txt', 'w', 'utf8')
text_tags = tt.TagText(sentences[0])
tagged_text = ""
for word in text_tags:
    tagged_text += word + "  "
f.write(tagged_text)
