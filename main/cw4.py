from lxml import etree
import nltk
from nltk.corpus import stopwords
import pymorphy2
import math

def sentences_from_xml(file):
    sentences = []
    tree = etree.parse(file)
    contents = tree.xpath('/Body/Sentence')
    for content in contents:
        sentences.append(content.text)
        print(content.text)
    return sentences

def normalize_words(words):
    morph = pymorphy2.MorphAnalyzer()
    stop_words = stopwords.words('russian')
    stop_words.extend(['что', 'это', 'так', 'вот', 'быть', 'как', 'в'])
    tokens = [i for i in words if (i not in stop_words)]
    norm_words = []
    for word in tokens:
        norm_words.append(morph.parse(word)[0].normal_form)
    return norm_words

def get_norm_sentences(sentences):
    norm_sentences =[]
    for sent in sentences:
        tokens =nltk.word_tokenize(sent)
        words = normalize_words(tokens)
        words = set(words)
        norm_sentences.append(words)
    return norm_sentences

def create_index(sentences):
    inverted_index = {}
    sent_number = 0;
    for sent in sentences:
        for word in sent:
            if inverted_index.get(word):
                inverted_index.get(word).append(sent_number)
            else :
                inverted_index.update({word: [sent_number]})
        sent_number += 1
    print("Index has built")
    return inverted_index

def creat_index_idf(sentences):
    idf_index = {}
    text_id =0;
    texts_idf = texs_idf(sentences)
    for text in texts_idf:
        for word, idf in text.items():
            if idf_index.get(word):
                idf_index.get(word).update({text_id: idf})
            else:
                idf_index.update({word: {text_id: idf}})
        text_id += 1
    return idf_index

def compute_idf(word, sentences):
    return math.log10(len(sentences) / sum([1.0 for i in sentences if word in i]))

def texs_idf(sentences):
    texts_list = []
    for text in sentences:
        idf = {}
        for word in text :
            idf[word] = compute_idf(word, sentences)
        texts_list.append(idf)
    return texts_list

def get_top10_texts(rank):
    return sorted(rank, key=rank.get, reverse=True)[:10]

file = 'health.xml'
#file = 'culture.xml'
sentences = get_norm_sentences(sentences_from_xml(file))
idf_index = creat_index_idf(sentences)

while True:
    query = input("Enter search query\n")
    search_words = normalize_words(nltk.word_tokenize(query))

    result = set()
    for word in search_words:
        found_words = idf_index.get(word)
        if found_words:
            if len(result) == 0:
                result = set(idf_index.get(word).keys())
            else:
                result = result.union(idf_index.get(word).keys())

    texts_rank_vector = {}
    for text_id in result:
        rank=0
        for word in search_words:
            if idf_index.get(word) and text_id in idf_index.get(word).keys():
                rank+=idf_index.get(word).get(text_id)
        texts_rank_vector.update({text_id: rank})

    top10 = get_top10_texts(texts_rank_vector)
    for i in top10:
        print(texts_rank_vector.get(i))
    print(top10)
    for i in top10:
        print (sentences[i])

