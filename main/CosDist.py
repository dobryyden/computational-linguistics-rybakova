import nltk
from  scipy import spatial
from InvertedIndex import sentences_from_xml, normalize_words, invert_index


def cosDist(cos_dist):
    return sorted(cos_dist, key=cos_dist.get, reverse=True)[:5]


file = 'culture.xml'
# file = 'health.xml'
sentences = sentences_from_xml(file)
inverted_index = invert_index(sentences)

while True:
    query = input("Enter search query:\n")
words = normalize_words(nltk.word_tokenize(query))

result = {}
for word in words:
    found_words = inverted_index.get(word)
    if found_words:
        if len(result) == 0:
            result = set(found_words)
        else:
            result = result.union(found_words)

vector = {}
for text in result:
    binVec = []
    for word in words:
        if text in inverted_index.get(word):
            binVec.append(1)
        else:
            binVec.append(0)
            vector.update({text: binVec})

queryVector = []
for word in words:
    if inverted_index.get(word):
        queryVector.append(1)
    else:
        queryVector.append(0)

cos_dist = {}
for text, binVec in vector.items():
    dist = spatial.distance.cosine(binVec, queryVector)
    cos_dist.update({text: dist})

print(cosDist(cos_dist))
