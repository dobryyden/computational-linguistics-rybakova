from lxml import etree
import re
import codecs
import os

directory1 = 'health'
directory2 = 'culture'

def xml_writer(directory):
    articles = os.listdir(directory)
    page = etree.Element('Body')
    for article in articles:
        t = codecs.open((directory + '/' + article), "r", "utf_8_sig").read()
        for sentence in t.split("."):
            title = etree.SubElement(page, 'Sentence')
            title.text = re.sub(r'[$«»–"!()\.,%/—\-:;\n\r]', "", sentence)
            with codecs.open(directory + '.xml', 'w', "utf_8_sig") as output:
             output.write(etree.tounicode(page , pretty_print=True))

xml_writer(directory1)
xml_writer(directory2)
